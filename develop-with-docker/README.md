# Docker, Docker-Composeを使った開発

## Backend
- Setup
```
$ docker-compose run --rm backend yarn install --no-optional
```

- lint
```
$ docker-compose run --rm backend yarn lint
$ docker-compose run --rm backend yarn lint:fix
```

- migration database
```
$ docker-compose run --rm backend yarn run typeorm migration:run
```

- test
```
$ docker-compose down -v                                            # コンテナが起動していたら落とす
$ docker-compose run --rm backend yarn run typeorm schema:sync      # テスト用にDBを初期化
$ docker-compose run --rm backend yarn test:coverage                # テスト実行
$ docker-compose down -v                                            # コンテナを停止（テストデータが入っているDBのボリュームもクリア）
```


--------

## Frontend
- Setup
```
$ docker-compose run --rm frontend yarn install --no-optional
```

- lint
```
$ docker-compose run --rm frontend yarn lint
```

--------


## Run service
- run
```
$ docker-compose up -d
```

- stop
```
$ docker-compose down
```

- if you want reset database, you can use `-v` option
```
$ docker-compose down -v
```

